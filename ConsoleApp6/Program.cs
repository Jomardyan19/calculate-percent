﻿using System;

namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Input number = ");
            int a = Convert.ToInt32(Console.ReadLine());
            Console.Write($"What percentage do you need to count out of {a} = ");
            int b = Convert.ToInt32(Console.ReadLine());
            Percent(a, b);
        }
        static void Percent(double a, double b)
        {
            double percent = a / 100 * b;
            Console.WriteLine($" Percent {b}% out of {a} = {percent}" );
        }
    }
}
